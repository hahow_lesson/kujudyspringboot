# Spring Boot 相關筆記 - 1

## 重點觀點:
1. spring 最重要的兩大觀念
    1. IoC(Inversion of Control): 控制反轉 => 將object的控制權交給spring容器
        - 降低耦合
        - 生命週期的管理
        - 方便測試
        - DI(Dependency Injection): 依賴注入 => 取得放在Spring容器中的Bean
    2. AOP(Aspect-Oriented Programming) 切面導向的程式
        - 當一個概念有在多個使用到相同的方法(邏輯)時，可將其提出來，進行一次的導入並且統一由切面處理
        - 可以只修改一個地方，降低程式維護的成本
        - 較常用在權限驗證(Spring security)，log紀錄，統一Exception處理(@ControllerAdvice)...
        - 至今很少用@Aspect直接實作，會使用人家包好的直接用，如上

## @Annotation(s)

1. @Autowired:
    - 根據變數的類型，去Spring容器中尋找有沒有符合類型的Bean
    - 將某一bean注入另一bean
2. @Component:
    - 將Class變成由Spring容器所管理的Object (稱之Bean)
    - Bean的命名方式是camelCase
3. @Qualifier:
    - 指定要注入的 Bean 的名字
4. @Configuration:
    - 只能加在class上
    - 用來設定spring用的
    - class名不重要，主要是內部的程式
    - 使用@Component所創建的Bean、以及使用@Configuration + @Bean所創建的Bean，他們會存放在同一個spring的容器裡
5. @Bean:
    - 放在有@Configuration的class中
    - 用在主要是在spring中創建一個bean
    - 所生成的bean就是方法所返回的object
    - bean的預設名字就是方法的名字
    - 或是在後方(""XXX")自訂義
6. @PostConstruct
    - 初始化bean
    - 用建立方法的方式建立，而其方法必為public並且返回void，且沒有parameter，方法名稱任意
    - 補充初始化Bean另一方法，使用InitializingBean並且@override afterPropertiesSet()的方法
    - 上述擇一進行
7. @Value
    - 加在Bean或是設定Spring用的class裡面的變數上，用來取得spring boot的設定檔(application.properties)
    - 使用@Value("${key}")
    - 類型是要一致的
    - 找不到key時可以預設值@Value("${unknowKey:defalutValue}")
8. @Aspect
    - 定義一個切面，一定要跟@Component一起使用
    - 用在class上，定義好後即可進行切方法
9. @Before/@After
    - 加在class的方法上，此方法的返回是void沒有parameter
    - 在切入點的方法前(@Before)/後(@After)執行
    - 用法是@Before("execution(* com.example.demo.CLASSNAME.*(..))") public void before() {...}，方法名稱可自訂
    - 用法是@After("execution(* com.example.demo.CLASSNAME.*(..))") public void after() {...}
    - 上述的切入的程之為切入點，切入點寫法繁多，使用前上網查即可
    - 要加入aop的dependency
10. @Around
    - 在切入點前後執行
    - 重點是 Object obj = pjp.proceed();
11. @AfterThrowing/@AfterReturning ...
    -  比較進階的用法可慢慢上網查詢

## Bean 的生命周期

* 創建 -> 初始化 -> 可以用(專指別人可以用)
    1. 要所有bean都可以被使用後application才可以起來
    2. 在創建Bean時若有依賴其他的Bean，則Spring會回過頭去創建且初始化那個被依賴的Bean
    3. bean會將依賴關係優先處理(因此要留意循環依賴的code)

## 讀取spring boot設定檔

* 設定檔的位置: src >> main >> resource >> application.properties
* 用key=value可以不用在=間加上空格
* 加上# 做為comment
* 通常工程開發的可調整的參數會寫在這，提供另一個比創建Bean還好用的方法
