# 電商網站

## 商品功能
* 查詢商品列表
* 新增/查詢/修改/刪除商品
  * 商品類型可以用enum的類型，用大寫定義且用底線來連接多個字詞，這主要提供了一個避免誤傳的功能也提供可讀性也可以做為debug用
  * Java的Date預設是格林威治時間，也因此可以透過application.properties來設定時區及時間返回格式
  * 查詢列表: 可用查詢條件(filtering)/排序(sorting)/分頁(pagination)
    * 查詢條件可以改用一個class來傳遞，減少改動到service/dao層了
    * 根據回傳的response body: 可以獨立回傳資料，或是可以再加入總筆數，簡單來說是改變回傳的格式，使用泛型來實作
  * 一般來說: 有用MockMvc來測就夠了，但是有複雜service再多測即可
    * 創建/更新/刪除建議加上@Transtional的注解

## 帳號功能(考慮資安的資訊較少)
* 註冊新帳號
* 登入
  * service層可以加入多個邏輯；而dao層是不能有的，僅能有與資料庫的互動
  * 儲存使用者的密碼時不應該直接存在DB，應用加密的方式
    * 常見的有用hash轉換出亂碼，而每一個字串轉換出來會是同一字串，就會有發生碰撞(collision)的可能(不同字轉出來一樣的可能性)
    * 而HASH嚴格上不是加密，因為無解密，常用的有：MD5，運作方式就是透過建立密碼的時候轉換的值與登入時轉換過的比對，一致時就可以登入
    * 雖然已被彩虹表(rainbow table)破解，但是還是可以透過加鹽(salt)來使用
    * 另種是對稱加密，只有一把KEY(加密/解密都同一把)
    * 還有一種是非對稱加密，公/私鑰各一把
  * 單元測試: H2Database的預設字有包含到user，所以要將user的table改成appuser
    * email也會被拆成e_mail，因此在jsonPath的地方要留意

## 訂單管理
* 創建訂單
  * order_item_table有存放product_id可以用join的方式來結合資訊 (SELECT * FROM order_item LEFT JOIN product ON order_item.product_id = product.product_id)
  * 而order_item.amount與order.total_amount的資訊是可以提供紀錄當下使用者在花費
  * 前端在傳可以使用json object來傳，也可以使用list直接傳，但是後者的擴充性比較低，並且熟悉json的巢狀object
  * 修改多張table時要記得加上@Transtional的注解
  * 使用batchUpdate一次性加入數據，效率更高
  * 有join的rowmapper的寫法: 可以採用創建一個new class或是用擴充的方式來接rowmapper
* 查詢訂單功能

## 未來擴充方向
* 商品功能: 優化查詢功能(Elastic Search)、權限管理RBAC(並非每人皆可進行更新、刪除)、price應該用decimal非int
* 帳號功能: 每個API的token
* 訂單功能: 多人搶票、狀態管理、串接金流