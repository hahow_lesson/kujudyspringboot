# Spring Boot 相關筆記 - 3

## Maven 相關知識: Maven 是在Spring boot開發中負責管理library及project構建的工具

1. Maven Library 管理: 就是透過pom.xml的檔案可以使用哪些功能
    - <dependencies></dependencies>中的<dependency></dependency>分別定義哪些功能
      - <dependency></dependency>中必要指定<groupId>公司的名字；<artifactId>是功能的名字；<version>指定版本
      - 如果dependency是spring-boot-starter-xxx就是由spring boot所開發的功能，不可以指定version，統一由<parent>中的<version>管理，避免誤用
      - maven的version管理: 以1.10.3為例是由主/次/增量版本，分別由重大/新功能/修bug的差別
      - 還有<scope>的標簽表示該功能的使用範圍，如: test就是代表只能用在單元測試中等等compile, test...皆為常用
      - <exclusions>中的<exclusion>是指不加載功能(library)的功能(library)
    - 查詢的方式: maven + xxx(功能)找對應的版本
    - Maven repository: 儲存spring boot project所用到的library，以jar檔的方式存在
      - 又分成local跟remote的maven repository, 會先以local的為優先取用，位置: ~/.m2/repository
      - remote repository可以是官方的也可以是私有的，並且可以連線到多個remote repository可以透過~/.m2/setting.xml來設定相關找循依據
      - 找循依據local一定是優先的
2. Maven Project 建構: 打包spring boot的程式
   - 透過pom.xml及相關指令進行打包，pom可在<parent></parent>與<dependency></dependency>間的進行設定
   - Maven指令分成3條生命周期，互不干擾且同一生命周期的指令會執行前面的指令，如default中的package指令會進行(1)->(2)->(3)
     - clean的生命周期 = (1*) 執行clean指令: 清除target的資料夾
     - default的生命周期 = (1) compile: 編譯spring boot + (2) test: 進行單元測試 + (3*) package: 打包成.jar檔，存在target資料夾 (4) install: 將.jar放到local repository中 (5)deploy: 將.jar放到remote repository
   - 補充上面: 
     - install打包完成後，可以讓別人載入後使用
     - 同時version中的後綴-SNAPSHOT(不穩定版)及-RELEASE(穩定版)，差在前者可以無限次上傳到remote repo.且可以被覆蓋，後者不行
     - 有加上-SNAPSHOT的就是不穩定版；不加的就是穩定版只能上傳一次且不能被覆蓋
3. application.properties的檔案可以用application-dev.properties及application-test.properties來設定區分dev及test的環境設定，但同時也要在啟動spring boot時要edit configuration中的activity profiles
4. log的使用，sl4j package中的使用log.info("xxxxx {}", variable);
   - log有三種等級區分: info, warn, error.
5. ObjectMapper: 將Json字串和Java Object互相作轉換，確保pom.xml有加上spring-boot-starter-web即可，
   - writeValueAsString(): Java object to Json，稱之serialize
   - readValue(): Json to Java object，稱之deserialize
   - 在沒有被java object所設定到的value，json預設是null，當然也可以透過在Object class上加入@JsonInclude(JsonInclude.Include.NON_NULL)來乎略
   - 若有一些json的key是在java object中沒有的，可以在Object class上加入@JsonIngnoreProperties(ignoreUnknown = true)
   - 因為在json變數的格式是採用_底線來連接，如: create_date，而在java object的變數名命方式有所不同(createDate)
     - 所以跟json的對應會對應不到，因此要在java變數上加上@JsonProperty("create_date") Date createDate;
     - 進一步學習: https://kucw.github.io/blog/2020/6/java-jackson/
6. 通常開發會透過前端作為REST請求方對spring boot發啟一個http請求，並且spring boot作為一個回應方
   - 但也可以讓spring boot作為REST請求方，對對外服務發啟一個請求，可以使用RestTemplate
   - 可以用 http://mocki.io 創建的API來測試，見code
7. Thymeleaf的用法: 是較為傳統的開發方式(使用者傳給後端，後端直接回傳HTML頁面)，目前也比較少用了`
   - thymeleaf就是spring boot的前端模板引擎，加人spring-boot-starter-thymeleaf
   - 其他還有JSP及Freemaker也是相同的
8. 3種注入bean的方式: 變數注入(Field Injection)、建構子注入(Constructor Injection)、Setter注入(Setter Injection)
   - 變數注入: 簡單但容易注入過多bean
   - 建構子注入: 容易了解依賴關係，但冗長，官方推薦
9. Spring Boot 2->3 的大改動
   - 只支援java 17之後的版本
   - 從Java EE遷移到Jakarta EE (javax.*的package移到jakarta.) 建議升級前寫好測項
   - 優化GraalVM技術 https://kucw.github.io/blog/2019/10/java-graalvm/