1. InterlliJ的實用技巧
   - option + Enter 萬用鍵: 提供一些對應的解法
   - command + shift + F: 全局搜循
   - command + option + <-: 往回看追源
   - control + option + O: 移除多餘的import
   - command + option + L: 美化程式碼
   - command + E: 顯示最近開的檔案
   - option + 滑鼠左下拉: 多行選取
   - load more than one spring boot: Load other spring boot pom.xml
2. Lombok: https://kucw.github.io/blog/2020/3/java-lombok/
3. Git的相關(On IntelliJ)
   - Remote, Local是不會自動存檔的，要自行的去push/pull
   - 在remote的brunch上會有一個前綴origin/，後面與local相同
   - 開發的過程中，會獨自建立一個branch，最後再合併合master/main branch
   - git的合併可以是用git merge或是提交一個pull request來合併，前者不建議使用，後者可以使用code review的方式讓其他人知道push了什麼code
     - 並且在merge的過程中，務必要注意合併的方向
     - 同時，pull request完後，要再自行pull到local repository中
   - 合併的過程中難免會遇到merge的conflict，要merge from remote的conflict的分支(通常是main/master)
     - 如何運用merge的技巧且不要改動到開發方向為大宗是學習merge的課題
     - 要記得需要時常將git status更新到最新的，來降低conflict的機率
   - 在pull request過程中會有一些建議，可以修改完後再push一次，讓程式更好
   - Git stash就是將一些未commit的code作為一些暫存
   - git merge: 將branch的commit與合併的branch結合後再commit出一個commit
   - git rebase: 將branch的所有commit結合到要合併的branch中
   - git squash: 將所有branch的commit合併成一個commit後結合到要合併的branch中
4. 雲端服務: AWS, Azure, GCP, Alibaba Cloud...透過DevOps工程師來部署、運行環境的維運
   - 延申學習: CI/CD自動化部署，Docker& Kubernetes及AWS證照...