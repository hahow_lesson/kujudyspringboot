package com.example.kujudyspringboot.controller;

import com.example.kujudyspringboot.dto.UserLoginRequest;
import com.example.kujudyspringboot.dto.UserRegisterRequest;
import com.example.kujudyspringboot.model.Appuser;
import com.example.kujudyspringboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/users/register")
    public ResponseEntity<Appuser> registor(@RequestBody @Valid UserRegisterRequest request) { // 以資安的考量
        Integer userId = userService.registor(request);

        Appuser appuser = userService.getUserById(userId);

        return ResponseEntity.status(HttpStatus.CREATED).body(appuser);
    }

    @PostMapping("/users/login")
    public ResponseEntity<Appuser> login(@RequestBody @Valid UserLoginRequest request) {
        Appuser appuser = userService.login(request);

        return ResponseEntity.status(HttpStatus.OK).body(appuser);

    }
}
