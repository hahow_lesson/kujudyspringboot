package com.example.kujudyspringboot.controller;

import com.example.kujudyspringboot.constant.ProductCategory;
import com.example.kujudyspringboot.dto.ProductQueryParams;
import com.example.kujudyspringboot.dto.ProductRequest;
import com.example.kujudyspringboot.model.Product;
import com.example.kujudyspringboot.service.ProductService;
import com.example.kujudyspringboot.util.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@Validated // 針對@RequestParam的驗證
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<Page<Product>> getProducts(
            // 查詢條件 filtering
            @RequestParam(required = false) ProductCategory category, // 用查看分類來filter，且required設為非必填
            @RequestParam(required = false) String search,

            // 排序 sorting
            @RequestParam(defaultValue = "created_date") String orderBy, // 根據什麼欄位排序且設預設為create_date
            @RequestParam(defaultValue = "DESC") String sort, // DESC or ASC且設預設為DESC

            // 分頁 pagination
            @RequestParam(defaultValue = "5") @Max(10) @Min(0)Integer limit, // 要取出幾筆數據
            @RequestParam(defaultValue = "0") @Min(0) Integer offset // 要跳過幾筆數據
    ) {
        ProductQueryParams params = new ProductQueryParams();
        params.setCategory(category);
        params.setSearch(search);
        params.setOrderBy(orderBy);
        params.setSort(sort);
        params.setLimit(limit);
        params.setOffset(offset);

        // 取得 product list
        List<Product> productList = productService.getProducts(params);

        // 取得總筆數
        Integer total = productService.countProduct(params);

        Page<Product> page = new Page<>();
        page.setLimit(limit);
        page.setOffset(offset);
        page.setTotal(total);
        page.setResults(productList);

        return ResponseEntity.status(HttpStatus.OK).body(page);
    }

    @GetMapping("/products/{productId}")
    public ResponseEntity<Product> getProduct(@PathVariable Integer productId) {
        Product product = productService.getProductById(productId);

        if (product != null)
            return ResponseEntity.status(HttpStatus.OK).body(product);
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    @PostMapping("/products")
    public ResponseEntity<Product> createProduct(@RequestBody @Valid ProductRequest request) {
        Integer productId = productService.createProduct(request);

        Product product = productService.getProductById(productId);

        return ResponseEntity.status(HttpStatus.CREATED).body(product);
    }

    @PutMapping("/products/{productId}")
    public ResponseEntity<Product> updateProduct(@PathVariable Integer productId,
                                                 @RequestBody @Valid ProductRequest request) {

        // check product exist.
        Product product = productService.getProductById(productId);
        if (product == null) return  ResponseEntity.status(HttpStatus.NOT_FOUND).build();

        // update product.
        productService.updateProduct(productId, request);

        Product updateProduct = productService.getProductById(productId);

        return ResponseEntity.status(HttpStatus.OK).body(updateProduct);
    }

    @DeleteMapping("/products/{productId}")
    public ResponseEntity<?> deleteProduct(@PathVariable Integer productId) {
        productService.deleteProductById(productId);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();

    }
}
