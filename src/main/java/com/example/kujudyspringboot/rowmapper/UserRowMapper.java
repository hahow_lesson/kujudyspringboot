package com.example.kujudyspringboot.rowmapper;

import com.example.kujudyspringboot.model.Appuser;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper implements RowMapper<Appuser> {
    @Override
    public Appuser mapRow(ResultSet resultSet, int i) throws SQLException {
        Appuser appuser = new Appuser();
        appuser.setUserId(resultSet.getInt("user_id"));
        appuser.setEmail(resultSet.getString("email"));
        appuser.setPassword(resultSet.getString("password"));
        appuser.setCreatedDate(resultSet.getTimestamp("created_date"));
        appuser.setLastModifiedDate(resultSet.getTimestamp("last_modified_date"));

        return appuser;
    }
}