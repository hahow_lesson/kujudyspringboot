package com.example.kujudyspringboot.dao.impl;

import com.example.kujudyspringboot.dao.ProductDao;
import com.example.kujudyspringboot.dto.ProductQueryParams;
import com.example.kujudyspringboot.dto.ProductRequest;
import com.example.kujudyspringboot.model.Product;
import com.example.kujudyspringboot.rowmapper.ProductRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ProductDaoImpl implements ProductDao {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public List<Product> getProducts(ProductQueryParams params) {
        String sql = """
                    SELECT product_id, product_name, category, image_url, price, stock, description, created_date, last_modified_date
                    FROM product WHERE 1=1
                """; // WHERE 1=1 is used to append sql schema
        Map<String, Object> map = new HashMap<>();

        // 查詢條件
        sql = addFilteringSql(sql, map, params);

        // 排序
        sql = sql + " ORDER BY " + params.getOrderBy() + " " + params.getSort(); // 可不用判定是否為null，因為前面有預防null

        // 分頁
        sql = sql + " LIMIT :limit OFFSET :offset";
        map.put("limit", params.getLimit());
        map.put("offset", params.getOffset());

        List<Product> productList = namedParameterJdbcTemplate.query(sql, map, new ProductRowMapper());
        return productList;
    }

    @Override
    public Integer countProduct(ProductQueryParams params) {
        String sql = """ 
            SELECT count(*) FROM product WHERE 1=1
        """;

        Map<String, Object> map = new HashMap<>();
        // 查詢條件
        sql = addFilteringSql(sql, map, params);

        Integer total = namedParameterJdbcTemplate.queryForObject(sql, map, Integer.class);
        return total;
    }

    @Override
    public void updateStock(Integer productId, Integer stock) {
        String sql = """
                UPDATE product SET stock = :stock, last_modified_date = :lastModifiedDate
                WHERE product_id = :productId
                """;
        Map<String, Object> map = new HashMap<>();
        map.put("productId", productId);
        map.put("stock", stock);
        map.put("lastModifiedDate", new Date());
        namedParameterJdbcTemplate.update(sql, map);
    }

    @Override
    public Product getProductById(Integer productId) {
        String sql = """
                   SELECT product_id, product_name, category, image_url, price, stock, description, created_date, last_modified_date
                   FROM product WHERE product_id = :productId;
                """;

        Map<String, Object> map = new HashMap<>();
        map.put("productId", productId);

        List<Product> productList = namedParameterJdbcTemplate.query(sql, map, new ProductRowMapper());

        if (productList.size() > 0)
            return productList.get(0);
        else
            return null;

    }

    @Override
    public Integer createProduct(ProductRequest productRequest) {
        String sql = """
                INSERT INTO product (product_name, category, image_url, price, stock, description, created_date, last_modified_date)
                VALUES (:productName, :category, :imageUrl, :price, :stock, :description, :createdDate, :lastModifiedDate);
                """;
        Map<String, Object> map = new HashMap<>();
        map.put("productName", productRequest.getProductName());
        map.put("category", productRequest.getCategory().toString());
        map.put("imageUrl", productRequest.getImageUrl());
        map.put("price", productRequest.getPrice());
        map.put("stock", productRequest.getStock());
        map.put("description", productRequest.getDescription());
        Date now = new Date();
        map.put("createdDate", now);
        map.put("lastModifiedDate", now);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource(map), keyHolder);
        int productId = keyHolder.getKey().intValue();
        return productId;
    }

    @Override
    public void updateProduct(Integer productId, ProductRequest request) {
        String sql = """
                UPDATE product SET 
                    product_name = :productName,
                    category = :category,
                    image_url = :imageUrl,
                    price = :price,
                    stock = :stock,
                    description = :description,
                    last_modified_date = :lastModifiedDate
                WHERE product_id = :productId
                """;

        Map<String, Object> map = new HashMap<>();
        map.put("productId", productId);
        map.put("productName", request.getProductName());
        map.put("category", request.getCategory().toString());
        map.put("imageUrl", request.getImageUrl());
        map.put("price", request.getPrice());
        map.put("stock", request.getStock());
        map.put("description", request.getDescription());
        map.put("lastModifiedDate", new Date());
        namedParameterJdbcTemplate.update(sql, map);
    }

    @Override
    public void deleteProductById(Integer productId) {
        String sql = """
                DELETE FROM product WHERE product_id = :productId
                """;

        Map<String, Object> map = new HashMap<>();
        map.put("productId", productId);
        namedParameterJdbcTemplate.update(sql, map);
    }

    private String addFilteringSql(String sql, Map<String, Object> map, ProductQueryParams params) {
        if (params.getCategory() != null) {
            sql = sql + " AND category = :category";
            map.put("category", params.getCategory().name());
        }

        if (params.getSearch() != null) {
            sql = sql + " AND product_name LIKE :search"; // remind that is needed a space in front of the statement.
            map.put("search", "%" + params.getSearch() + "%");
        }

        return sql;
    }

}
