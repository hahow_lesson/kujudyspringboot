package com.example.kujudyspringboot.dao.impl;

import com.example.kujudyspringboot.dao.UserDao;
import com.example.kujudyspringboot.dto.UserRegisterRequest;
import com.example.kujudyspringboot.model.Appuser;
import com.example.kujudyspringboot.rowmapper.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class UserDaoImpl implements UserDao {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Appuser getUserById(Integer userId) {
        String sql = """
                SELECT user_id, email, password, created_date, last_modified_date
                FROM appuser WHERE user_id = :userId
                """;
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        List<Appuser> appuserList = namedParameterJdbcTemplate.query(sql, map, new UserRowMapper());
        if (appuserList.size() > 0)
            return appuserList.get(0);
        else
            return null;
    }

    @Override
    public Integer createUser(UserRegisterRequest userRegisterRequest) {
        String sql = """
                INSERT INTO appuser (email, password, created_date, last_modified_date)
                VALUES (:email, :password, :createdDate, :lastModifiedDate)
                """;
        Map<String, Object> map = new HashMap<>();
        map.put("email", userRegisterRequest.getEmail());
        map.put("password", userRegisterRequest.getPassword());
        Date now = new Date();
        map.put("createdDate", now);
        map.put("lastModifiedDate", now);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource(map), keyHolder);
        int userId = keyHolder.getKey().intValue();
        return userId;
    }

    @Override
    public Appuser getUserByEmail(String email) {
        String sql = """
                SELECT user_id, email, password, created_date, last_modified_date
                FROM appuser WHERE email = :email
                """;
        Map<String, Object> map = new HashMap<>();
        map.put("email", email);
        List<Appuser> appuserList = namedParameterJdbcTemplate.query(sql, map, new UserRowMapper());
        if (appuserList.size() > 0)
            return appuserList.get(0);
        else
            return null;
    }


}
