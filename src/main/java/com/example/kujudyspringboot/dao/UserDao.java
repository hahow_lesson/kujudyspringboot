package com.example.kujudyspringboot.dao;

import com.example.kujudyspringboot.dto.UserRegisterRequest;
import com.example.kujudyspringboot.model.Appuser;

public interface UserDao  {
    Appuser getUserById(Integer userId);

    Integer createUser(UserRegisterRequest request);

    Appuser getUserByEmail(String email);
}
