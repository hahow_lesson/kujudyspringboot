package com.example.kujudyspringboot.service;

import com.example.kujudyspringboot.constant.ProductCategory;
import com.example.kujudyspringboot.dto.ProductQueryParams;
import com.example.kujudyspringboot.dto.ProductRequest;
import com.example.kujudyspringboot.model.Product;

import java.util.List;

public interface ProductService {
    Product getProductById(Integer productId);

    Integer createProduct(ProductRequest request);

    void updateProduct(Integer productId, ProductRequest request);

    void deleteProductById(Integer productId);

    List<Product> getProducts(ProductQueryParams params);

    Integer countProduct(ProductQueryParams params);
}
