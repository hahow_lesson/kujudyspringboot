package com.example.kujudyspringboot.service;

import com.example.kujudyspringboot.dto.CreateOrderRequest;
import com.example.kujudyspringboot.dto.OrderQueryParams;
import com.example.kujudyspringboot.model.Order;

import java.util.List;

public interface OrderService {
    Integer createOrder(Integer userId, CreateOrderRequest request);

    Order getOrderById(Integer orderId);

    List<Order> getOrders(OrderQueryParams orderQueryParams);

    Integer countOrder(OrderQueryParams orderQueryParams);
}
