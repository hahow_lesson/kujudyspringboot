package com.example.kujudyspringboot.service.impl;

import com.example.kujudyspringboot.dao.UserDao;
import com.example.kujudyspringboot.dto.UserLoginRequest;
import com.example.kujudyspringboot.dto.UserRegisterRequest;
import com.example.kujudyspringboot.model.Appuser;
import com.example.kujudyspringboot.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.web.server.ResponseStatusException;

@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    private final static Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    public Appuser getUserById(Integer userId) {
        return userDao.getUserById(userId);
    }

    @Override
    public Integer registor(UserRegisterRequest request) {
        // 驗證是否已註冊
        Appuser appuser = userDao.getUserByEmail(request.getEmail());
        if (appuser != null) {
            log.warn("Using the duplicated email: {}", request.getEmail());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        // Using MD5
        String hashedPassword = DigestUtils.md5DigestAsHex(request.getPassword().getBytes());
        request.setPassword(hashedPassword);

        return userDao.createUser(request);
    }

    @Override
    public Appuser login(UserLoginRequest request) {
        Appuser appuser = userDao.getUserByEmail(request.getEmail());
        if (appuser == null) {
            log.warn("Not register: {}", request.getEmail());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        // Using MD5
        String hashedPassword = DigestUtils.md5DigestAsHex(request.getPassword().getBytes());

        if (appuser.getPassword().equals(hashedPassword)) {
            return appuser;
        } else {
            log.warn("Wrong password: {}", request.getEmail());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

    }
}
