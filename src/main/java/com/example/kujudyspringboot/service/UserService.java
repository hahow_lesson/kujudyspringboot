package com.example.kujudyspringboot.service;

import com.example.kujudyspringboot.dto.UserLoginRequest;
import com.example.kujudyspringboot.dto.UserRegisterRequest;
import com.example.kujudyspringboot.model.Appuser;

public interface UserService {
    Appuser getUserById(Integer userId);

    Integer registor(UserRegisterRequest request);

    Appuser login(UserLoginRequest request);
}
