package com.example.kujudyspringboot.constant;

public enum ProductCategory {
    FOOD,
    CAR,
    COMPUTER_ACCESSORY,
    BOOK
}
