package com.example.kujudyspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KuJudySpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(KuJudySpringbootApplication.class, args);
    }

}
